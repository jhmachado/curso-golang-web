package main

import (
	"log"
	"os"
	"text/template"
)

type region struct {
	Name   string
	Hotels []hotel
}

type hotel struct {
	Name, Address, City, Zip string
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	regions := []region{
		{
			Name: "Southern",
			Hotels: []hotel{
				{"Hotel 1", "Address 1", "City 1", "Zip 1"},
				{"Hotel 2", "Address 2", "City 2", "Zip 2"},
				{"Hotel 3", "Address 3", "City 3", "Zip 3"},
				{"Hotel 4", "Address 4", "City 4", "Zip 4"},
			},
		},
		{
			Name: "Central",
			Hotels: []hotel{
				{"Hotel 5", "Address 5", "City 5", "Zip 5"},
				{"Hotel 6", "Address 6", "City 6", "Zip 6"},
				{"Hotel 7", "Address 7", "City 7", "Zip 7"},
				{"Hotel 8", "Address 8", "City 8", "Zip 8"},
			},	
		},
		{
			Name: "Northern",
			Hotels: []hotel{
				{"Hotel 9", "Address 9", "City 9", "Zip 9"},
				{"Hotel 10", "Address 10", "City 10", "Zip 10"},
				{"Hotel 11", "Address 11", "City 11", "Zip 11"},
				{"Hotel 12", "Address 12", "City 12", "Zip 12"},
			},	
		},
	}

	err := tpl.Execute(os.Stdout, regions)
	if err != nil {
		log.Fatalln(err)
	}
}
