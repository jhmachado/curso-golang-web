package main

import "fmt"

var y int

type human interface {
	speak()
}

type person struct {
	fname string
	lname string
}

func (p person) speak() {
	fmt.Println(p.fname, `says, "Good morning, James."`)
	fmt.Println(p.lname, `says, "Good morning, James."`)
}

type secretAgent struct {
	person
	licenseToKill bool
}

func (sa secretAgent) speak() {
	fmt.Println(sa.fname, sa.lname, `says, "Sharen, not stirred."`)
}

func saySomething(h human) {
	h.speak()
}

func main() {
	// x := 7
	// fmt.Printf("%T", x)
	// fmt.Println(y)

	// xi := []int{2,4,7,9,42}
	// fmt.Println(xi)

	// m := map[string]int{
	// 	"Todd": 45,
	// 	"Job": 42,
	// }

	// fmt.Println(m)

	p1 := person{
		"Miss",
		"Moneypenny",
	}

	// fmt.Println(p1)
	// p1.speak()

	sa1 := secretAgent{
		person{
			"James",
			"Bond",
		},
		true,
	}

	// sa1.speak()
	// sa1.person.speak()

	saySomething(p1)
	saySomething(sa1)
}
