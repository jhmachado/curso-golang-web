package main

import (
	"log"
	"os"
	"text/template"
)

type meal struct {
	Name   string
	Items  []item
}

type item struct {
	Name, Price string
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	menu := []meal{
		{
			Name: "Breakfast",
			Items: []item{
				{"Item 1", "Price $1,00"},
				{"Item 2", "Price $2,00"},
				{"Item 3", "Price $3,00"},
				{"Item 4", "Price $4,00"},
			},
		},
		{
			Name: "Lunch",
			Items: []item{
				{"Item 5", "Price $5,00"},
				{"Item 6", "Price $6,00"},
				{"Item 7", "Price $8,00"},
				{"Item 9", "Price $10,00"},
			},
		},
		{
			Name: "Dinner",
			Items: []item{
				{"Item 1", "Price $1,00"},
				{"Item 2", "Price $2,00"},
				{"Item 3", "Price $3,00"},
				{"Item 4", "Price $4,00"},
			},	
		},
	}

	err := tpl.Execute(os.Stdout, menu)
	if err != nil {
		log.Fatalln(err)
	}
}
